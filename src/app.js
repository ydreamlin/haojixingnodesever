const express = require('express')
const bodyParser = require('body-parser')
const log = require('./middlewares/log')
const access = require('./middlewares/access')
const ignoreFavicon = require('./middlewares/ignore-favicon')
const trunApiPrefix = require('./middlewares/trun-apiprefix')

const { API_PREFIX, PORT } = require('../config')

require('./utils/db')

const app = express()

// 解析body
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

app.use(ignoreFavicon())                     // 忽略 favicon 请求
app.use(log())                               // 打印请求的参数、方式、数据
app.use(access())                            // 解决跨越问题
app.use(trunApiPrefix(API_PREFIX))           // 统一处理请求前缀

const router = require('./router')
router(app)

const server = app.listen(PORT, function () {
    console.log('Listening on port', server.address().port)
})