var user = require('./routers/user')
var cashbook = require('./routers/cashbook')
var detail = require('./routers/detail')
const weapp = require('./routers/weapp')

module.exports = function (app) {
    app.get('/', function (req, res) { res.send('hello world') })

    app.post('/cashbook', cashbook.addCashbook)
    app.delete('/cashbook/:id', cashbook.deleteCashbook)
    app.put('/cashbook/:id', cashbook.editCashbook)
    app.get('/cashbooks', cashbook.getCashbookList)

    app.post('/detail', detail.postDetail)
    app.delete('/detail/:id', detail.deleteDetail)
    app.put('/detail/:id', detail.putDetail)
    app.get('/details', detail.getDetailList)
    app.get('/detail/:id', detail.getDetail)

    app.put('/user', user.putUser)
    app.get('/user', weapp.login)
    
}