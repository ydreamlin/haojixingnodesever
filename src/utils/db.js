var mysql = require('mysql')
const dbConfig = require('../../config')

var db = mysql.createPool({
  host     : dbConfig.DB_HOST,
  user     : dbConfig.DB_USER,
  password : dbConfig.DB_PASSWORD,
  database : dbConfig.DB_DATABASE,
  port: dbConfig.DB_PORT,
  connectionLimit: 10,
})

// db.connect((err) => {
//     if (err)  {
//         console.log('Connected to database is Failed!')
//         console.log(err.message)
//         return
//     }
//     console.log('Connected to database')
// })

global.dbexec = function (sql, params) {
    return new this.Promise(function (resolve, reject) {
        db.query(sql, params, (err, result) => {
            if (err) reject({ code: '9999', message: err })
            resolve({ code: '0000', message: '操作成功', data: result})
        })
    })
}