const moment = require('moment')
const chalk = require('chalk')

module.exports = function () {
    return function (req, res, next) {
        console.log('\n\n')
        const now = moment(new Date()).format('YYYY-MM-DD HH:mm:ss SSS')
        console.log(`
Timer: ${chalk.green(now)} ——【${chalk.green(req.method)}  ${chalk.green(req.originalUrl)}】
请求参数(req.query)：${chalk.green(JSON.stringify(req.query))}   
请求参数(req.body)：${chalk.green(JSON.stringify(req.body))}
        `)
        console.log('\n\n')
        next()
    }
}