const cashbookDao = require('../dao/cashbook')

module.exports =  {
    /**
     * 新增记账本
     */
    addCashbook: async function (req, res) {
        const { userId, name } = req.body

        if (!userId) {
            return res.json({ code: '9999', message: '用户ID(userId)不能为空', data: {} })
        }

        if (!name) {
            return res.json({ code: '9999', message: '账本名称(name)不能为空', data: {} })
        }

        try {
            const result = await cashbookDao.saveCashbook({ belongUserId: userId, name })
            console.log('[+] routes > cashbook > addCashbook()', result.data)
            res.json({ code: '0000', message: '新增账本操作成功', data: {} })
        } catch (e) {
            console.log('[-] routes > cashbook > addCashbook()', e.message)
            res.json({ code: '9999', message: '新增账本操作失败', data: {} })
        }
    },

    /**
     * 删除记账本
     * 只能删自己的账本，不能删参与的账本
     */
    deleteCashbook: async function (req, res) {
        const { id } = req.params
        const { userId } = req.body

        if (!id) {
            return res.json({ code: '9999', message: '账本ID(id)不能为空', data: {} })
        }

        if (!userId) {
            return res.json({ code: '9999', message: '用户ID(userId)不能为空', data: {} })
        }

        try {
            const getResult = await cashbookDao.getCashbookList({ coopUserId: userId })
            const cashbookList = getResult.data
            const cashbook = cashbookList.find(item => String(item.id) === String(id)) || {}
            if (String(cashbook.belong_user_id) !== String(userId)) {
                return res.json({ code: '9999', message: '您不是账本的创建人', data: {} })
            }

            const result = await cashbookDao.deleteCashbook({ id })
            console.log('[+] routes > cashbook > deleteCashbook()', result.data)
            res.json({ code: '0000', message: '删除账本操作成功', data: {} })
        } catch (e) {
            console.log('[-] routes > cashbook > deleteCashbook()', e.message)
            res.json({ code: '9999', message: '删除账本操作失败', data: {} })
        }
    },

    /**
     * 修改记账本名称
     */
    editCashbook: async function (req, res) {
        const  { id } = req.params
        const { name } = req.body

        if (!name) {
            return res.json({ code: '9999', message: '账本名称(name)不能为空', data: {} })
        }

        try {
            const result = await cashbookDao.updateCashbook({ name, id })
            console.log('[+] routes > cashbook > editCashbook()', result.data)
            res.json({ code: '0000', message: '修改账本操作成功', data: {} })
        } catch (e) {
            console.log('[-] routes > cashbook > editCashbook()', e.message)
            res.json({ code: '9999', message: '修改账本操作失败', data: {} })
        }
    },

    /**
     * 查询账本列表
     */
    getCashbookList: async function (req, res) {
        const { coopUserId } = req.query

        try {
            const result = await cashbookDao.getCashbookList({ coopUserId })
            res.json(result)
        } catch (e) {
            console.log('[-] routes > cashbook > getCashbookList()', e.message)
            res.json({ code: '9999', message: '查询账本操作失败', data: {} })
        }   
    },
}