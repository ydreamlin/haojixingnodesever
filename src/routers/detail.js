const detailDao = require('../dao/detail')

module.exports = {
     /**
      * 新增明细
      */
     postDetail: async function (req, res) {
          const { userId, userAvatar, type, amt, remark, picUrl, amtType, cashbookId, creatDate } = req.body

          if (!userId) {
               return res.json({ code: '9999', message: '用户ID(userId)不能为空', data: {} })
          }

          if (!cashbookId) {
               return res.json({ code: '9999', message: '账本ID(cashbookId)不能为空', data: {} })
          }

          if (!amt) {
               return res.json({ code: '9999', message: '金额(amt)不能为空', data: {} })
          }

          try {
               const result = await detailDao.saveDetail({ cashbookId, userAvatar, userId, type, amt, remark, picUrl, amtType, creatDate })
               console.log('[+] routes > detail > postDetail', result.data)
               res.json({ code: '0000', message: '新增明细成功', data: {} })
          } catch (e) {
               console.log('[-] routes > detail > postDetail', e.message)
               res.json({ code: '9999', message: '新增明细失败', data: {} })
          }
     },

     /**
      * 删除明细
      */
     deleteDetail: async function (req, res) {
          const { id } = req.params

          try {
               const result = await detailDao.deleteDetail({ id })
               console.log('[+] routes > detail > deleteDetail', result.data)
               res.json({ code: '0000', message: '删除明细成功', data: {} })
          } catch (e) {
               console.log('[-] routes > detail > deleteDetail', e.message)
               res.json({ code: '9999', message: '删除明细失败', data: {} })
          }
     },

     /**
      * 修改明细
      */
     putDetail: async function (req, res) {
          const { id } = req.params
          const { amt, remark, picUrl, type } = req.body

          try {
               const result = await detailDao.updateDetail({ amt, remark, picUrl, type, id })
               console.log('[+] routes > detail > putDetail', result.data)
               res.json({ code: '0000', message: '修改明细成功', data: {} })
          } catch (e) {
               console.log('[-] routes > detail > putDetail', e.message)
               res.json({ code: '9999', message: '修改明细失败', data: {} })
          }
     },

     /**
      * 查询明细列表
      */
     getDetailList: async function (req, res) {
          const { cashbookId, year, month } = req.query

          if (!cashbookId) {
               return res.json({ code: '9999', message: '账本ID(cashbookId)不能为空', data: {} })
          }

          try {
               const result = await detailDao.getDetailList({ cashbookId, year, month })
               res.json(result)
          } catch (e) {
               console.log('[-] routes > detail > getDetailList', e.message)
               res.json({ code: '9999', message: '查询明细列表失败', data: {} })
          }
     },

     /**
      * 根据id查看明细
      */
     getDetail: async function(req, res) {
          const { id } = req.params
          const result = await detailDao.getDetail({ id }) 
          return res.json(result)
     }
}