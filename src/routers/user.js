const userDao = require('../dao/user')

module.exports = {
    putUser: async function (req, res) {
        try {
            const { nickname, avatarUrl, id ,sex  } = req.body
            await userDao.updateUser({ nickname, sex, avatarUrl, id })
            res.json({
                code: '0000',
                message: '操作成功',
                data: {
                    nickname,
                    sex,
                    avatarUrl,
                    id,
                },
            })
        } catch (e) {
            console.log('[-] routes > user > putUser()', e.message)
            res.json({ code: '9999', message: '修改用户信息失败', data: {} })
        }
    },
}  