module.exports = {
    /**
     * 新增用户
     */
    saveUser: async function ({ username, password }) {
        const sql = 'insert into user (username, password, create_time) values (?, ?, now())'
        const result = await dbexec(sql, [username, password])
        return result
    },

    /**
     * 修改用户
     */
    updateUser: async function ({ nickname, sex, avatarUrl, id }) {
        let sql = 'update user set last_update_time = now()'
        if (nickname) {
            sql += `, nickname = "${nickname}" `
        }
        if (sex) {
            sql += `, sex = "${sex}" `
        }
        if (avatarUrl) {
            sql += `, avatar_url = "${avatarUrl}" `
        }
        sql += `where id = ${id}`
        const result = await dbexec(sql)
        return result
    },
    
    /**
     * 查询用户信息
     */
    getUser: async function ({ username, password }) {
        let sql = 'select id, nickname, avatar_url from user where 1 = 1'

        if (username) {
            sql += ` and username = "${username}"`
        }
        if (password) {
            sql += ` and password = "${password}"`
        }

        const result = await dbexec(sql)
        result.data = result.data[0] || {}
        return result
    },
}