module.exports = {
    /**
     * 保存记账本
     */
    saveCashbook: async function ({ belongUserId, name = '默认账本' }) {
        const sql = 'insert into cashbook (belong_user_id, coop_user_id, name, create_time) values (?, ?, ?, now())'
        const result = await dbexec(sql, [belongUserId, belongUserId, name])
        return result
    },

    /**
     * 更新记账本
     */
    updateCashbook: async function ({ name, coopUserId, id }) {
        let sql = 'update cashbook set last_update_time = now()'   
        if (coopUserId) {
            sql += `, coop_user_id = ${coopUserId} `
        }
        if (name) {
            sql += `, name = "${name}" `
        }
        sql += `where id = ${id}`
        const result = await dbexec(sql)
        return result
    },

    /**
     * 查询用户所有账本
     */
    getCashbookList: async function ({ coopUserId }) {
        let sql = `SELECT id, name, coop_user_id, belong_user_id FROM cashbook where coop_user_id LIKE '%${coopUserId}%'` 
        const result = await dbexec(sql)
        return result
    },

    /**
     * 删除记账本
     */
    deleteCashbook: async function ({ id }) {
        let sql = `delete from cashbook where id = ${id}`
        const result = await dbexec(sql)
        return result
    },

}